package pl.edu.pwsztar;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

final class UserId implements UserIdChecker {

    private final String id;    // NR. PESEL

    public UserId(final String id) {
        this.id = id;
    }

    @Override
    public boolean isCorrectSize() {
        return this.id != null && id.length() == 11;
    }

    @Override
    public Optional<Sex> getSex() {
        if (isCorrect()) {
            if (Character.getNumericValue(id.charAt(9)) % 2 == 0) {
                return Optional.of(Sex.WOMAN);
            } else {
                return Optional.of(Sex.MAN);
            }
        } else {
            return Optional.empty();
        }
    }

    @Override
    public boolean isCorrect() {
        if(!isCorrectSize() || !id.matches("[0-9]+")) {
            return false;
        }

        int[] checksumDigits = new int[] {9,7,3,1,9,7,3,1,9,7};
        int sum = 0;
        for(int i = 0; i < 10 ; i++) {
            sum += Character.getNumericValue(this.id.charAt(i)) * checksumDigits[i];
        }

        return sum % 10 == Character.getNumericValue(this.id.charAt(10));
    }

    @Override
    public Optional<String> getDate() {
        if(!isCorrect()) {
            return Optional.empty();
        }

        int year = 10 * Character.getNumericValue(this.id.charAt(0)) + Character.getNumericValue(this.id.charAt(1));
        int month = 10 * Character.getNumericValue(this.id.charAt(2)) + Character.getNumericValue(this.id.charAt(3));
        int day = 10 * Character.getNumericValue(this.id.charAt(4)) + Character.getNumericValue(this.id.charAt(5));

        if (month > 80 && month < 93) {
            month -= 80;
            year += 1800;
        } else if (month > 20 && month < 33) {
            month -= 20;
            year += 2000;
        } else if (month > 40 && month < 53) {
            month -= 40;
            year += 2100;
        } else if (month > 60 && month < 73) {
            month -= 60;
            year += 2200;
        }  else if (month > 0 && month < 13) {
            year += 1900;
        }

        try {
            return Optional.of(LocalDate.of(year, month, day).format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
        } catch(Exception e) {
            return Optional.empty();
        }
    }
}
