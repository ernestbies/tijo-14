package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class UserIdSpec extends Specification {

    @Unroll
    def "should check correct size for ID: #id"() {
        given:
            def userId = new UserId(id)
        when:
            def isCorrectSize = userId.isCorrectSize()
        then:
            isCorrectSize == expected
        where:
            id                  | expected
            '86051489399'       | true
            '48090645796'       | true
            '66013192499'       | true
            '6601319249'        | false
            '660131924'         | false
            ''                  | false
            null                | false
    }

    @Unroll
    def "should get correct sex for ID: #id"() {
        given:
            def userId = new UserId(id)
        when:
            def userIdSex = userId.getSex()
        then:
            userIdSex.get() == expected
        where:
            id                  | expected
            '86051489399'       | UserIdChecker.Sex.MAN
            '48090645796'       | UserIdChecker.Sex.MAN
            '66013192499'       | UserIdChecker.Sex.MAN
            '75042642761'       | UserIdChecker.Sex.WOMAN
            '57070194924'       | UserIdChecker.Sex.WOMAN
            '81043016788'       | UserIdChecker.Sex.WOMAN
    }

    @Unroll
    def "should not get correct sex for ID: #id"() {
        given:
            def userId = new UserId(id)
        when:
            def userIdSex = userId.getSex()
        then:
            userIdSex == expected
        where:
            id                  | expected
            '123456789'         | Optional.empty()
            ''                  | Optional.empty()
            null                | Optional.empty()
    }

    @Unroll
    def "should check if ID: #id is correct"() {
        given:
            def userId = new UserId(id)
        when:
            def isCorrect = userId.isCorrect()
        then:
            isCorrect == expected
        where:
            id                  | expected
            '86051489399'       | true
            '48090645796'       | true
            '66013192499'       | true
            '76012363952'       | true
            '00312869865'       | true
            '57112894157'       | true
            '61021392835'       | true
            '68092746943'       | true
            '47092977232'       | true
            '99101082446'       | true
            '48100136465'       | true
            'abcdefgijkl'       | false
            '6601319249'        | false
            '660131924'         | false
            ''                  | false
            null                | false
    }

    @Unroll
    def "should get birthdate from ID: #id"() {
        given:
            def userId = new UserId(id)
        when:
            def date = userId.getDate()
        then:
            date.get() == expected
        where:
            id                  | expected
            '86051489399'       | '14-05-1986'
            '48090645796'       | '06-09-1948'
            '66013192499'       | '31-01-1966'
            '67100691389'       | '06-10-1967'
            '00210796764'       | '07-01-2000'
            '09281998523'       | '19-08-2009'
            '20221283433'       | '12-02-2020'
    }

    def "should not get birthdate from ID"() {
        given:
            def userId = new UserId(id)
        when:
            def date = userId.getDate()
        then:
            date == expected
        where:
            id                  | expected
            '01021378911'       | Optional.empty()
            '8605148939'        | Optional.empty()
            ''                  | Optional.empty()
            null                | Optional.empty()
    }
}
